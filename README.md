## Pwn Firewall
[![Build status](https://gitlab.com/zjw88282740/pwn-firewall/badges/master/build.svg)](https://gitlab.com/zjw88282740/pwn-firewall/commits/master)

A CTF tool
  * [1]log stdin and stdout.
  * [2]diable some syscall to protect your gamebox.

## Building and Installing
You should follow the steps below.

Firstly, you should install the libseccomp library.

	# sudo apt install libseccomp2 libseccomp-dev

Second, config the tool.

  If you want to use seccmop to disable syscall

	# define SECC 1

  else if you want to use ptrace, add a line below

	# undef SECC

  You can set the target binary name and log name in the sources.

	# const char *log_name = "waf_log";
	# const char *elf_name = "./pwn";

Last, build from sources.

	# make

## License

[The MIT License (MIT)](https://gitlab.com/zjw88282740/pwn-firewall/raw/master/LICENSE)


## Credits

This repo relies on the following third-party projects:

* In production:
  * [DrimTuer/bin-waf](https://github.com/DrimTuer/bin-waf)
  * [Asuri-Team/pwn-sandbox](https://github.com/Asuri-Team/pwn-sandbox)
  * [seccomp/libseccomp](https://github.com/seccomp/libseccomp)